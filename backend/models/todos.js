const mongoose = require('mongoose')

// Называние схем всегда во множественном числе
// Ключи вашей схемы крайне опасно менять/удалять
const Todos = mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    isDone: {
        type: Boolean
    }
})

module.exports = mongoose.model('Todos', Todos)
