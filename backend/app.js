// Импортируем наш фреймворк
const express = require("express")
// Создаем главную переменную
const app = express()
const bodyParse = require("body-parser")
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = 7777

// Middlewares
app.use(cors())
app.use(bodyParse.json())

// Routes
app.use("/todos", require("./routes/todos"))

// Подключение к БД
mongoose.connect('mongodb://localhost:27017/myapp')
    .then(() => console.log("Мы подключились к БД"))
    .catch(err => console.log(err));

mongoose.connection.on('error', err => {
    console.log(err);
})

// Эта строка будет оповещать о любых действиях с БД
mongoose.set("debug", true)

// Слушатель по порту
app.listen(PORT, () => {
    console.log(`Сервер слушает порт ${PORT}`)
})
