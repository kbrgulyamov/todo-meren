const express = require("express")
const router = express.Router()
// Подключаем модель к раутеру
const Todos = require("../models/todos")

// Создание одной записи
router.post("/", async (req, res) => {
    // Тут идет метод создания записи в БД
    try {

        const todos = await Todos.create(req.body)
        console.log(todos);
        return res.status(201).json({
            ok: true,
            message: "Element created!",
            // Значение берем из аргумента функции
            // Тут мы получаем только что созданный элемент
            element: todos
        })

    } catch (error) {
        console.log(error);

        res.status(500).json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

// Получение всех записей (Асинхронная функция)
router.get("/", async (req, res) => {
    try {
        // Найти все данные
        let body = await Todos.find()

        // Эта запись ждет предыдущюю
        res.json({
            ok: true,
            message: "Get all todos!",
            body
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

module.exports = router
